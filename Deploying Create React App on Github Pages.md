---


---

<h2 id="github-pages"><a href="https://pages.github.com/">GitHub Pages</a></h2>
<blockquote>
<p>Note: this feature is available with  <code>react-scripts@0.2.0</code>  and higher.</p>
</blockquote>
<h3 id="step-1-add--homepage--to--package.json"><a href="https://create-react-app.dev/docs/deployment#step-1-add-homepage-to-packagejson"></a>Step 1: Add  <code>homepage</code>  to  <code>package.json</code></h3>
<p><strong>The step below is important!</strong></p>
<p><strong>If you skip it, your app will not deploy correctly.</strong></p>
<p>Open your  <code>package.json</code>  and add a  <code>homepage</code>  field for your project:</p>
<pre class=" language-json"><code class="prism  language-json">  <span class="token string">"homepage"</span><span class="token punctuation">:</span> <span class="token string">"https://myusername.github.io/my-app"</span><span class="token punctuation">,</span>

</code></pre>
<p>or for a GitHub user page:</p>
<pre class=" language-json"><code class="prism  language-json">  <span class="token string">"homepage"</span><span class="token punctuation">:</span> <span class="token string">"https://myusername.github.io"</span><span class="token punctuation">,</span>

</code></pre>
<p>or for a custom domain page:</p>
<pre class=" language-json"><code class="prism  language-json">  <span class="token string">"homepage"</span><span class="token punctuation">:</span> <span class="token string">"https://mywebsite.com"</span><span class="token punctuation">,</span>

</code></pre>
<p>Create React App uses the  <code>homepage</code>  field to determine the root URL in the built HTML file.</p>
<h3 id="step-2-install--gh-pages--and-add--deploy--to--scripts--in--package.json"><a href="https://create-react-app.dev/docs/deployment#step-2-install-gh-pages-and-add-deploy-to-scripts-in-packagejson"></a>Step 2: Install  <code>gh-pages</code>  and add  <code>deploy</code>  to  <code>scripts</code>  in  <code>package.json</code></h3>
<p>Now, whenever you run  <code>npm run build</code>, you will see a cheat sheet with instructions on how to deploy to GitHub Pages.</p>
<p>To publish it at  <a href="https://myusername.github.io/my-app">https://myusername.github.io/my-app</a>, run:</p>
<pre class=" language-sh"><code class="prism  language-sh">npm install --save gh-pages

</code></pre>
<p>Alternatively you may use  <code>yarn</code>:</p>
<pre class=" language-sh"><code class="prism  language-sh">yarn add gh-pages

</code></pre>
<p>Add the following scripts in your  <code>package.json</code>:</p>
<pre class=" language-diff"><code class="prism  language-diff">  "scripts": {
<span class="token inserted">+   "predeploy": "npm run build",</span>
<span class="token inserted">+   "deploy": "gh-pages -d build",</span>
    "start": "react-scripts start",
    "build": "react-scripts build",

</code></pre>
<p>The  <code>predeploy</code>  script will run automatically before  <code>deploy</code>  is run.</p>
<p>If you are deploying to a GitHub user page instead of a project page you’ll need to make one additional modification:</p>
<ol>
<li>Tweak your  <code>package.json</code>  scripts to push deployments to  <strong>master</strong>:</li>
</ol>
<pre class=" language-diff"><code class="prism  language-diff">  "scripts": {
    "predeploy": "npm run build",
<span class="token deleted">-   "deploy": "gh-pages -d build",</span>
<span class="token inserted">+   "deploy": "gh-pages -b master -d build",</span>

</code></pre>
<h3 id="step-3-deploy-the-site-by-running--npm-run-deploy"><a href="https://create-react-app.dev/docs/deployment#step-3-deploy-the-site-by-running-npm-run-deploy"></a>Step 3: Deploy the site by running  <code>npm run deploy</code></h3>
<p>Then run:</p>
<pre class=" language-sh"><code class="prism  language-sh">npm run deploy

</code></pre>
<h3 id="step-4-for-a-project-page-ensure-your-project’s-settings-use--gh-pages"><a href="https://create-react-app.dev/docs/deployment#step-4-for-a-project-page-ensure-your-project-s-settings-use-gh-pages"></a>Step 4: For a project page, ensure your project’s settings use  <code>gh-pages</code></h3>
<p>Finally, make sure  <strong>GitHub Pages</strong>  option in your GitHub project settings is set to use the  <code>gh-pages</code>  branch:</p>
<p><img src="https://i.imgur.com/HUjEr9l.png" alt="gh-pages branch setting"></p>
<h3 id="step-5-optionally-configure-the-domain"><a href="https://create-react-app.dev/docs/deployment#step-5-optionally-configure-the-domain"></a>Step 5: Optionally, configure the domain</h3>
<p>You can configure a custom domain with GitHub Pages by adding a  <code>CNAME</code>  file to the  <code>public/</code>  folder.</p>
<p>Your CNAME file should look like this:</p>
<pre><code>mywebsite.com

</code></pre>
<h3 id="notes-on-client-side-routing"><a href="https://create-react-app.dev/docs/deployment#notes-on-client-side-routing"></a>Notes on client-side routing</h3>
<p>GitHub Pages doesn’t support routers that use the HTML5  <code>pushState</code>  history API under the hood (for example, React Router using  <code>browserHistory</code>). This is because when there is a fresh page load for a url like  <code>http://user.github.io/todomvc/todos/42</code>, where  <code>/todos/42</code>  is a frontend route, the GitHub Pages server returns 404 because it knows nothing of  <code>/todos/42</code>. If you want to add a router to a project hosted on GitHub Pages, here are a couple of solutions:</p>
<ul>
<li>You could switch from using HTML5 history API to routing with hashes. If you use React Router, you can switch to  <code>hashHistory</code>  for this effect, but the URL will be longer and more verbose (for example,  <code>http://user.github.io/todomvc/#/todos/42?_k=yknaj</code>).  <a href="https://reacttraining.com/react-router/web/api/Router">Read more</a>  about different history implementations in React Router.</li>
<li>Alternatively, you can use a trick to teach GitHub Pages to handle 404 by redirecting to your  <code>index.html</code>  page with a special redirect parameter. You would need to add a  <code>404.html</code>  file with the redirection code to the  <code>build</code>  folder before deploying your project, and you’ll need to add code handling the redirect parameter to  <code>index.html</code>. You can find a detailed explanation of this technique  <a href="https://github.com/rafrex/spa-github-pages">in this guide</a>.</li>
</ul>
<h3 id="troubleshooting"><a href="https://create-react-app.dev/docs/deployment#troubleshooting"></a>Troubleshooting</h3>
<h4 id="devtty-no-such-a-device-or-address"><a href="https://create-react-app.dev/docs/deployment#dev-tty-no-such-a-device-or-address"></a>“/dev/tty: No such a device or address”</h4>
<p>If, when deploying, you get  <code>/dev/tty: No such a device or address</code>  or a similar error, try the following:</p>
<ol>
<li>Create a new  <a href="https://github.com/settings/tokens">Personal Access Token</a></li>
<li><code>git remote set-url origin https://&lt;user&gt;:&lt;token&gt;@github.com/&lt;user&gt;/&lt;repo&gt;</code>  .</li>
<li>Try  <code>npm run deploy</code>  again</li>
</ol>
<h4 id="cannot-read-property-email-of-null"><a href="https://create-react-app.dev/docs/deployment#cannot-read-property-email-of-null"></a>“Cannot read property ‘email’ of null”</h4>
<p>If, when deploying, you get  <code>Cannot read property 'email' of null</code>, try the following:</p>
<ol>
<li><code>git config --global user.name '&lt;your_name&gt;'</code></li>
<li><code>git config --global user.email '&lt;your_email&gt;'</code></li>
<li>Try  <code>npm run deploy</code>  again</li>
</ol>

