
# What is DeFi? Quick 5 min starter snapshot

##### Background:
This article has been prepared with the objective that you are a complete NOOB with the actions happening in the decentralized/blockchain/bitcoin/ crypto world.   This article will provide you with some quick bullet pointers to the most recent and hottest topic in the decentralized world: `DeFi` 


## Plain Vanilla Definition

DeFi is made up of 2 words - Decentralised + Finance

## So what does that mean actually???
Well, in a simple sense it means 
-	the ability to lend (similar to a deposit) crypto assets (eg cryptocurrency) with `smart contracts` and
-	the ability to borrow crypto assets (eg cryptocurrency) with `smart contracts` 

I mention cryptocurrency as an eg because the blockchain world has something known as the `NFT` {for simplicity purposes, I will not discuss NFTs in this article} which also play a role in the DeFi world.

## A little bit more detailed

So DeFi is like imitating (this is an over-over-simplification) the traditional financial ecosystem into the blockchain world.  The DeFi space has started with vanilla products like:

1. Deposit your crypto and earn interest in crypto
2. Borrow crypto by collateralizing your crypto assets



![](https://miro.medium.com/max/2634/1*wgBtV-8nVF3P55Obekelqw.png)


<!--stackedit_data:
eyJoaXN0b3J5IjpbMTgwMjU0NDk1OV19
-->