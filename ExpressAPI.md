---


---

<ol>
<li>Hit the api of token set and get the price of the asset (api inside github) (eg we get back usd 300)</li>
<li>Hit the api for coingecko to get the price for ETH/USD (<a href="https://api.coingecko.com/api/v3/simple/price?ids=ethereum&amp;vs_currencies=usd">https://api.coingecko.com/api/v3/simple/price?ids=ethereum&amp;vs_currencies=usd</a>)</li>
<li>we have the total ETH that the user wants to invest (this is the input from given by the USER in the front end)</li>
<li>get the value of the  ETH using step 2</li>
<li>do 25% of step 4 to get determine the value in USD that will be invested in TS (eg we get 100USD)</li>
<li>output will be (ans of step 5)/(ans of step 1) = eg ans is 0.33333333</li>
<li>we will convert this into a bignumber for 18 decimals and without float</li>
<li>this result is what Dipesh will call in his SC</li>
</ol>
<p>The above steps will change a bit where the USER directly inputs the USD amount that he wants to invest, rather than the ETH value.  In that case, we will NOT need to determine the step 5; rather we will directly do a 25% of that amount and get our ans for step 5.</p>
<p>expected result:<br>
<code>{ "qty": "XXXX", }</code></p>

