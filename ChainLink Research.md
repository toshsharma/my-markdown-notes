---


---

<h4 id="chainlink-provides-the-ability-for-smart-contracts-to-be-connected-to-real-world">Chainlink provides the ability for smart contracts to be connected to real world:</h4>
<ul>
<li>data,</li>
<li>events and</li>
<li>payments.</li>
</ul>
<h4 id="setup-is-quick-and-easy-forindividual-developers-and-enterprises.">Setup is quick and easy forindividual developers and enterprises.</h4>
<p>By operating a Chainlink node, you’re able to offer any API for direct purchase by smart contracts. Once your Chainlink node receives a request from a smart contract, it fulfills that request using your existing APIs, allowing you to sell your services to the large &amp; growing blockchain economy.</p>
<h4 id="any-developer-can-quickly-build-and-launch-their-own-chainlink-to-sell-any-api-to-smart-contracts">Any developer can quickly build and launch their own Chainlink to sell any API to smart contracts</h4>
<p>By creating a new Chainlink as a developer, you’ll be paid by making something thousands of smart contracts will rely on.</p>
<h4 id="docs">Docs</h4>
<p>Chainlink will provide your smart contract with access to any external API you want to connect your smart contract with, and all the information needed to do so are listed here.</p>
<h4 id="where-do-i-start">Where do I start?</h4>
<p>Your starting point depends on what role you have in the Chainlink ecosystem. The options listed below will get you set up in the right place:</p>
<ul>
<li>Click  <a href="https://docs.chain.link/docs/contract-creators-overview">here</a>  if you want to  <strong>build a smart contract that uses Chainlink</strong></li>
<li>Click  <a href="https://docs.chain.link/docs/node-operator-overview">here</a>  if you want to  <strong>operate a Chainlink node</strong></li>
<li>Click  <a href="https://docs.chain.link/docs/external-adapters">here</a>  if you want to  <strong>build an external adapter</strong></li>
</ul>
<h4 id="requesting-data">Requesting data</h4>
<p>In order to create requests, you will need to specify parameters for the Chainlink node to use when processing your job. For details on which parameters to specify, see our  <a href="https://docs.chain.link/docs/adapters">Adapters</a>  page. Specifying parameters is accomplished by using methods within  <code>Chainlink.Request</code>  which takes a key (as a string) and value. We have the following method available for different value types:</p>
<ul>
<li><code>add</code>: Adds a  <code>string</code></li>
<li><code>addBytes</code>: Adds  <code>bytes</code></li>
<li><code>addInt</code>: Adds a  <code>int256</code></li>
<li><code>addUint</code>: Adds a  <code>uint256</code></li>
<li><code>addStringArray</code>: Adds an array of strings</li>
</ul>
<p>In the examples below, we specify a  <code>"get"</code>  key for the  <a href="https://docs.chain.link/docs/adapters#section-httpget">HttpGet</a>  adapter, a  <code>"path"</code>  key for the  <a href="https://docs.chain.link/docs/adapters#section-jsonparse">JsonParse</a>  adapter, and a  <code>"times"</code>  key for the  <a href="https://docs.chain.link/docs/adapters#section-multiply">Multiply</a>  adapter.</p>
<blockquote>
<p>Written with <a href="https://stackedit.io/">StackEdit</a>.</p>
</blockquote>

