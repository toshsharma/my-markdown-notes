## 6 October 2019

Eg:
MakerDAO = 200$
Borrowing Power of  200$ / 150% = 130$ from MakerDAO = DAI$
I borrow = 50$
if the value of the ETH falls to 75$
MakerDAO will sell it and collect 75$

#### Point 3 about Angelo - It is about stop loss order, which the exchanges have alredy done it.

### Question: in what account is the savings being locked?
Ans:  It should be locked in the `NEWSC`

### Question: What is the currency of the savings?
Ans:  DAI







Flow of events

(A) every days 15 days {event}
(a) Angelo says pay 10 ETH to Dipesh
 { - APP will issue msg.send (fx to transfer ETH} to Dipesh
  - Metamask will ask for permission
  - Metamask will send the 10 ETH to Dipesh}
(c) { App will also ask for another tx (msg.transfer to 0.1 ETH) to the `NEWSC`
	 - NEWSC will supply the ETH to Compound and starts earning ETH
	 - after 15 days interest is collected from Compound
	 - on the 15th day: 0.01 ETH AND the interest is trasferred back to Angelo to his wallet

Complexity 1

(a) During the 15 days it is not just Angelo, but Angelo1 to Angelo100, the `NEWSC` is pooling the savings (For tosh, a calendar needs to be displayed)
on the 16th day, pooled amount is supplied to Compound
the next 16 to 30 days, interest is earned
on the 31st day interest _plus_ the principal is returned to all the Angelo1 to Angelo100

Complexity 2
- people are going to use all the currencies
- KNC, or any other token:
	- instruct Metamask to do the transfer
	- instruct metamask to collect the 1% savings and collect it into the `NEWSC`
- `NEWSC` is going to use Kyber to convert anything other than DAI$ to DAI$, in real time
<!--stackedit_data:
eyJoaXN0b3J5IjpbMTczMjI0MjMyNV19
-->