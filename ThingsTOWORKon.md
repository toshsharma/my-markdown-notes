---


---

<ol>
<li>MUST MUST MUST HAVE is to move the web3js to the correct zap</li>
<li>MUST HAVE - After showing the correct branch, put a note to redirect the user to the Lender Zap</li>
<li>MUST HAVE - API for the GasPrice should be called at the click of the second Buy Button</li>
<li>MUST HAVE - Link to the SmartContract folder in the landingPage (Dipesh added the folder in Tosh’s branch)</li>
<li>HomePage - NavBar (everywhere) “DeFi Zap” insert Beta over there too (superscript) | <em>Must have</em></li>
<li>Move the “Get Results” button right below the last question | <em>Must have</em></li>
<li>Thinking should be below the “Get Results” | Good to have</li>
<li>Once Tx submitted - show the modal that tx submitted - MUST HAVE</li>
<li>Once Tx Confirmed - show a modal that the tx has submitted!!  MUST HAVE</li>
<li>In the results page:</li>
</ol>
<ul>
<li>Show the card first | Good to Have</li>
<li>Show the Redo button below and the Explore All Zaps | Good to Have</li>
</ul>
<ol start="11">
<li>3 Speed Buttons below the buy to change the gas price automatically | Good to have</li>
<li>Visit (a) Fulcrum Trade website [with the link] and (b) Compound [with the link] to find out your balances | Good to have</li>
<li>web3js functionality has been TESTED</li>
</ol>
<h4 id="version-2-review--315pm-singapore-time">Version 2 Review @ 315pm Singapore Time</h4>
<ol>
<li>Too much space in the left | Good2Have</li>
<li>Why Use Defi Zap section - not center aligned | Must have</li>
<li>View our smart contracts - not linked | Must have</li>
<li>Landing Page Zap - Buy button not working | Must have</li>
<li>“Get Results” Button needs to go below the last question | Good2Have</li>
<li>Results is still a sentence not a card | must have</li>
<li></li>
</ol>

